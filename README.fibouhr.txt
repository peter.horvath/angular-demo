Testaufgabe

1. Aufgabe
Eine Fibonacci-Uhr stellt die Uhrzeit durch die ersten fünf Elemente der Fibonacci-Folge dar.
Das „Ziffernblatt“ der Uhr besteht dabei aus fünf Quadraten, dessen Seitenlängen den
ersten fünf Zahlen dieser Reihe entsprechen (1, 1, 2, 3, 5).
Dabei werden die Stunden durch rote und die Minuten durch grüne Quadrate dargestellt.
Soll ein Quadrat die Stunde und die Minuten darstellen, ist es blau. Weiße Quadrate werden
ignoriert und nicht zur Berechnung der Uhrzeit herangezogen. Um nun die aktuelle Uhrzeit
ablesen zu können, muss man die Werte der verschiedenen Quadrate addieren. Um die
Stunde zu berechnen werden die Wertigkeiten aller roten und blauen Quadrate addiert. Für
die Minuten dagegen die Grünen und Blauen. Das Ergebnis der Minuten muss dann noch mit
5 multipliziert werden um das richtige Ergebnis zu erhalten.
Zu beachten ist, dass es für einige Uhrzeiten mehr als eine Darstellungsvariante gibt. Die
zufällig gewählt werden kann. Zum Beispiel kann die Uhrzeit 6:30 Uhr auf 16 verschiedene
Arten in einer Fibonacci-Uhr dargestellt werden.
Erstellen sie eine SPA, die eine einfache Version einer Fibonacci-Uhr visualisiert. Die
Anwendung soll über einen weiter- und einen zurück-Button verfügen, der die aktuelle Zeit
um 5 Minuten vor- bzw. zurückstellt. Dabei soll die Uhrzeit auf der Fibonacci-Uhr und als
Text angezeigt werden. Gibt es verschiedene Darstellungsvarianten für die Uhrzeit, kann
diese zufällig gewählt werden – es kann aber auch immer die Gleiche sein.

2. Hinweise
2.1 Technologien
Verwenden Sie zur Umsetzung der Aufgabe folgende Technologien
* Angular ( >= v8.x, mit TypeScript)
* CSS3 oder SASS (mit einem beliebigen CSS-Framework)
* NPM (>= v.6.x ) und NodeJS (>= v12.x)
* Powershell oder eine beliebige Unix-Shell

2.2 Randbedingungen
* Die zuletzt gewählte Uhrzeit soll nach einem Refresh der WebApp erhalten bleiben.
* Dabei soll die Persistierung unabhängig von der „anzeigenden“ Komponente
arbeiten und jederzeit einfach austauschbar sein. Für eine schnelle
Implementierung kann der Persistierungsservice den Local- oder Sessionstorage
des Browsers verwenden.
* Teilen Sie die App sinnvoll in Komponenten und Services auf um ein späteres
Erweitern/Refactoren zu vereinfachen.
* Erstellen Sie Unit-Tests mit einer beliebigen Test-Library und einem beliebigen
Testrunner, die mindestens eine Komponente und einen Service der App testen.

Erstellen sie eine kurzes Shell- oder NPM-Script (im root-Ordner der App), dass die
Anwendung im Produktiv-Modus baut und die fertigen Artefakte so bereitstellt,
dass Sie im Browser via http-Protokoll auf einem lokalen Port ausgeliefert werden
und angezeigt werden können.
