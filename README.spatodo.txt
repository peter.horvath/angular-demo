Testaufgabe

1. Aufgabe

Erstellen Sie eine einfache SPA zum verwalten einer TODO-Liste. Die einzelnen TODOEinträge sollen dabei mindestens folgende Eigenschaften besitzen:
* Beschreibung
* Status (offen o. erledigt)
* Fälligkeitsdatum (Anzeige: DD.MM.YYYY)

Der Nutzer soll folgende Aktionen durchführen können:
* Anzeige aller Einträge
* Anlegen eines neuen Eintrages
* Löschen eines bestehenden Eintrages
* Ändern des Status eines Eintrages (offen nach erledigt, aber nicht umgekehrt)

2. Hinweise
2.1 Technologien
Verwenden Sie zur Umsetzung der Aufgabe folgende Technologien
* Angular ( >= v8.x, mit TypeScript)
* CSS3 oder SASS (mit einem beliebigen CSS-Framework)
* NPM (>= v.6.x ) und NodeJS (>= v12.x)
* Powershell oder eine beliebige Unix-Shell

2.2 Randbedingungen
* Die Einträge sollen als Chips/Tags dargestellt werden (s. Beispiel)
* Der Status soll entweder durch eine Checkbox oder durch einfach Auswählen des
Eintrages erfolgen. Der Status soll dabei die Hintergrundfarbe des Chips
widerspiegeln.
* Auf „großen“ Bildschirmen sollen jederzeit sechs Einträge je Reihe angezeigt
werden. Auf „mittleren“ Bildschirmgrößen drei je Reihe und auf mobilen Geräten,
sollen alle Einträge untereinander angezeigt werden.

Die Gesamtbreite aller Einträge je Reihe soll sich immer über die gesamte
verfügbare Breite des Bildschirmes erstrecken.

Alle Einträge sollen persistiert werden, also nach dem Refresh der Seite erhalten
bleiben. Die Persistierung soll unabhängig von der „anzeigenden“ Komponente
arbeiten und jederzeit einfach austauschbar sein. Für eine schnelle
Implementierung kann der Persistierungsservice den Local- oder Sessionstorage
des Browsers verwenden.

Teilen Sie die App sinnvoll in Komponenten und Services auf um ein späteres
Erweitern/Refactoren zu vereinfachen.

Erstellen Sie Unit-Tests mit einer beliebigen Test-Library und einem beliebigen
Testrunner, die mindestens eine Komponente und einen Service der App testen.
Erstellen sie eine kurzes Shell- oder NPM-Script (im root-Ordner der App), dass die
Anwendung im Produktiv-Modus baut und die fertigen Artefakte so bereitstellt,
dass Sie im Browser via http-Protokoll auf einem lokalen Port ausgeliefert werden
und angezeigt werden können.
