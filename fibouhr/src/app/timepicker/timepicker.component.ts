import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

interface IntOpt {
  value: number,
  viewValue: string
};

@Component({
  selector: 'app-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.less']
})
export class TimepickerComponent implements OnInit {
  hours : number[] = [];
  minutes : number[] = [];

  hour: number = 0;
  min: number = 0;

  constructor() {
    for (let h = 0; h < 12; h++) {
      this.hours[h]=h;
    }
    for (let m = 0; m * 5 < 60; m++) {
      this.minutes[m] = m * 5;
    }
    try {
      let hourStr = localStorage.getItem("hour");
      if (hourStr != null) {
        this.hour = parseInt(hourStr);
      }
      let minStr = localStorage.getItem("min");
      if (minStr != null) {
        this.min = parseInt(minStr);
      }
    } catch (e) {
      console.log(e);
    }
  }

  ngOnInit(): void {
  }

  changeHour(event: Event) {
    this.hour = parseInt((event.target as HTMLSelectElement).value);
    this.refreshClock();
  }

  changeMin(event: Event) {
    this.min = parseInt((event.target as HTMLSelectElement).value);
    this.refreshClock();
  }

  refreshClock() {
    localStorage.setItem('hour', this.hour.toString());
    localStorage.setItem('min', this.min.toString());
  }
}
