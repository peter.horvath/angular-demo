import { Component, ViewChild, AfterViewInit } from '@angular/core';

import { TimepickerComponent } from './timepicker/timepicker.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('appTimePicker') timePicker !: TimepickerComponent;

  ngAfterViewInit() {
  }
}
