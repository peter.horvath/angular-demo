import { Component, OnInit, Input } from '@angular/core';

let tileValue : number[] = [1, 1, 2, 3, 5];

let done : boolean = false;

let tileColors : string[] = [];

// Tile colors: 0: white, 1: red (adds to hours), 2: green (adds to min), 3: blue (adds to hours and min)
let colors : string[] = ["white", "red", "green", "blue"];

function generateTileColors() {
  if (done) {
    return tileColors;
  }
  for (let a:number = 0; a < 1024; a++) {
    let tileColor : number;
    let hrs : number = 0;
    let min : number = 0;
    let curColors : string[] = [];
    for (let b:number = 0; b < 5; b++) {
      tileColor = (a >> (b*2)) & 0x3;
      curColors[b] = colors[tileColor];
      if (tileColor == 1 || tileColor == 3) {
        hrs += tileValue[b];
      }
      if (tileColor == 2 || tileColor == 3) {
        min += tileValue[b];
      }
    }
    for (let b:number = 0; b < 5; b++) {
      tileColors[hrs * 60 + min * 5 + b] = curColors[b];
    }
  }
  done = true;
  console.log(tileColors);
  return tileColors;
}

let fiboDig: boolean[][] = [
  // 1      1      2      3      5
  [false, false, false, false, false], // 0
  [true,  false, false, false, false], // 1
  [false, false, true,  false, false], // 2
  [false, false, false, true,  false], // 3
  [true,  true,  true,  false, false], // 4
  [false, false, false, false, true ], // 5
  [true,  false, false, false, true ], // 6
  [false, false, true,  false, true ], // 7
  [false, false, false, true,  true ], // 8
  [true,  false, false, true,  true ], // 9
  [false, false, true,  true,  false], // 10
  [true,  false, true,  true,  true ]  // 11
];

@Component({
  selector: 'app-tiles',
  templateUrl: './tiles.component.html',
  styleUrls: ['./tiles.component.less']
})
export class TilesComponent implements OnInit {
  tileColors:string[] = [];

  @Input('hour') hour:number = 0;
  @Input('min') min:number = 0;

  constructor() {
    this.tileColors = generateTileColors();
  }

  ngOnInit(): void {
  }

  tileColor(hour: number, min: number, pos: number) {
    let minIdx = Math.floor(min/5);
    let hourOn = fiboDig[hour][pos];
    let minOn = fiboDig[minIdx][pos];
    return colors[(hourOn?1:0) + (minOn?2:0)];
  }
}
