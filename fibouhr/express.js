// very simple static file service of the dist/fibouhr - useful to simplify prod testing

var express = require('express');
var app = express();

app.use('/', express.static('dist/fibouhr'));

app.listen(4200);
