import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

import { TasksService } from '../tasks.service';
import { Task } from '../task';
import { getTomorrow } from '../util';

@Component({
  selector: 'app-new-task-dialog',
  templateUrl: './new-task-dialog.component.html',
  styleUrls: ['./new-task-dialog.component.less']
})
export class NewTaskDialogComponent implements OnInit {

  model : Task = new Task(null, '', false, getTomorrow());

  constructor(private dialogRef: MatDialogRef<NewTaskDialogComponent>, private tasksService: TasksService) { }

  ngOnInit(): void {
  }

  closeDialog(saveResult: boolean) {
    // TODO this.tasksService.buildTask
    console.log('closeDialog: ' + this.model.description);
    if (saveResult) {
      this.tasksService.buildTask(this.model.description, false, this.model.dueDate);
      this.tasksService.saveTasks();
    }
    this.dialogRef.close(saveResult);
  }
}
