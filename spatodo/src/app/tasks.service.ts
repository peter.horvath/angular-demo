import { Injectable } from '@angular/core';
import { Task } from './task';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  private tasks : Map<number, Task> = new Map<number, Task>();
  private nextId : number = 1;

  constructor() {
    this.loadTasks();
  }

  loadTasks() {
    try {
      let tasksStr : string | null = localStorage.getItem("tasks");
      if (!tasksStr) {
        throw new Error("no tasks in persistent storage");
      }
      var unvTasks = JSON.parse(tasksStr);
      if (!unvTasks || typeof unvTasks !== "object") {
        throw new Error("localStorage.tasks is not an array");
      }
      for (const [_unvTaskId, _unvTask] of Object.entries(unvTasks)) {
        const unvTaskId = parseInt(_unvTaskId);

        if (isNaN(unvTaskId)) {
          throw new Error("localStorage.tasks has non-integer id " + _unvTaskId);
        }

        const unvTask = _unvTask as Task;

        console.log(typeof unvTask.dueDate);

        if (unvTask == null
          || typeof unvTask !== "object"
          || !unvTask.hasOwnProperty("description")
          || !unvTask.hasOwnProperty("isDone")
          || !unvTask.hasOwnProperty("dueDate")
          || typeof unvTask.description !== "string"
          || typeof unvTask.isDone !== "boolean"
          //|| isNaN(Date.parse(unvTask.dueDate))
        )
          {
          throw new Error("localStorage.tasks[" + unvTaskId + "] " + JSON.stringify(unvTask) + " is invalid");
        }

        let newTask : Task = new Task(null, unvTask.description, unvTask.isDone, unvTask.dueDate);
        console.log("adding newTaskId " + unvTaskId);
        this.tasks.set(unvTaskId, newTask);
        if (unvTaskId + 1 >= this.nextId) {
          this.nextId = unvTaskId + 1;
        }
      }
    } catch(e: any) {
      console.log(e.toString());
      this.tasks = new Map<number, Task>();
    }
  }

  buildTask(description: string, isDone: boolean, dueDate: Date) {
    let newTask : Task = new Task(null, description, isDone, dueDate);
    this.tasks.set(this.nextId, newTask);
    this.nextId++;
  }

  saveTasks() {
    var json: any = {};
    this.tasks.forEach((task: Task, id: number) => {
      json[id] = task.toJson();
    });
    localStorage.setItem("tasks", JSON.stringify(json));
    console.log(JSON.stringify(json));
  }

  setDone(id: number) {
    let task = this.tasks.get(id);
    if (task) {
      task.isDone = true;
    }
  }

  delTask(id: number) {
    this.tasks.delete(id);
  };

  getTasksList() : Task[] {
    let ret : Task[] = [];
    for (let [id, task] of this.tasks) {
      ret.push(task.cloneWithId(id));
    }
    return ret;
  }
}
