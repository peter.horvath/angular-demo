export function getTomorrow() : Date {
  let tomorrow: Date = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  return tomorrow;
}
