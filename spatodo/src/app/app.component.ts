import { Component, Directive, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NewTaskDialogComponent } from './new-task-dialog/new-task-dialog.component';
import { TasksTableComponent } from './tasks-table/tasks-table.component';

@Directive({selector: 'tasksTable'})
class TasksTableDirective {
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements AfterViewInit {
  title = 'spatodo';

  @ViewChild('tasksTable') tasksTable !: TasksTableComponent;

  constructor(public newTaskDialog: MatDialog) {}

  openNewTaskDialog() {
    const newTaskDialogRef = this.newTaskDialog.open(NewTaskDialogComponent);
    newTaskDialogRef.afterClosed().subscribe(result => {
      this.tasksTable.refresh();
    });
  };

  ngAfterViewInit() {
  };
}
