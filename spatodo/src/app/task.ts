export class Task {
  id: number | null;
  description: string;
  isDone: boolean;
  dueDate: Date;

  constructor(id: number | null, description: string, isDone: boolean, dueDate: Date) {
    this.id = id;
    this.description = description;
    this.isDone = isDone;
    this.dueDate = dueDate;
  }

  toJson() {
    var ret : any = new Object();
    ret.description = this.description;
    ret.isDone = this.isDone;
    ret.dueDate = this.dueDate;
    return ret;
  }

  cloneWithId(id: number) {
    return new Task(id, this.description, this.isDone, this.dueDate);
  }
}
