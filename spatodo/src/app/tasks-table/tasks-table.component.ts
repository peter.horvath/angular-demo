import { Component, OnInit } from '@angular/core';
import { Task } from '../task';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-tasks-table',
  templateUrl: './tasks-table.component.html',
  styleUrls: ['./tasks-table.component.less']
})
export class TasksTableComponent implements OnInit {
  displayedColumns: string[] = ['description', 'dueDate', 'status'];
  tasks : Task[] = [];

  constructor(private tasksService : TasksService) { }

  ngOnInit() {
    this.refresh();
  }

  clickDone(id: number) {
    this.tasksService.setDone(id);
    this.tasksService.saveTasks();
    this.refresh();
  }

  clickDel(id: number) {
    this.tasksService.delTask(id);
    this.tasksService.saveTasks();
    this.refresh();
  }

  refresh() {
    this.tasks = this.tasksService.getTasksList();
  }
}
